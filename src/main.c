#include "t_boite.h"       
#include "t_disjoncteur.h"

#include <stdio.h>

int main()
{
	// Creates struct on the stack.
	t_boite boite;
	t_disjoncteur disjoncteur[NB_LIGNES_MAX][NB_COLONNES];

	// Loop on all NB_LIGNES_MAX and NB_COLONNES.	
	for (size_t lineIdx = 0; lineIdx < NB_LIGNES_MAX; lineIdx++)
	{
		for (size_t colonneIdx= 0; colonneIdx < NB_COLONNES; colonneIdx++ )
		{
			// Assigne boite tab_disjoncteurs pointer to disjoncteur array on the stack.
			boite.tab_disjoncteurs[lineIdx][colonneIdx] = &disjoncteur[lineIdx][colonneIdx];

			// Add same values to all disjoncteur.
			boite.tab_disjoncteurs[lineIdx][colonneIdx]->ampere = 3.0;
			boite.tab_disjoncteurs[lineIdx][colonneIdx]->etat = 4;
			boite.tab_disjoncteurs[lineIdx][colonneIdx]->demande_du_circuit = 5.0;
			boite.tab_disjoncteurs[lineIdx][colonneIdx]->tension = 6;

			// Print all boite disjoncteur values.
			printf("boite disjoncteur[%zu][%zu]\n", lineIdx, colonneIdx);
			printf("[ampere:%f][etat:%d][demande_du_circuit:%f][tension:%d]\n",
				   boite.tab_disjoncteurs[lineIdx][colonneIdx]->ampere,
				   boite.tab_disjoncteurs[lineIdx][colonneIdx]->etat,
				   boite.tab_disjoncteurs[lineIdx][colonneIdx]->demande_du_circuit,
				   boite.tab_disjoncteurs[lineIdx][colonneIdx]->tension);
		}
	}

	return 0;
}
