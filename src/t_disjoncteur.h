/*
* Module de type qui regroupe tout dans le projet qui concerne un
* disjoncteur.
*
* On y retrouve les constantes, d�finition de type et sous-programme
* li� � un disjoncteur.
*/
#ifndef __T_DISJONCTEUR__
#define __T_DISJONCTEUR__

// �tats de l'interrupteur d'un disjoncteur.
#define ALLUME 1 // � l'installation.
#define ETEINT 0 // surcharge de courant.

// Les diff�rentes tensions
#define TENSION_PHASE 120
#define TENSION_ENTREE 240

// Tous les amp�rages consid�r�s pour un disjoncteur.
const int AMPERAGES_PERMIS[] = {15, 20, 40, 50, 60};

// Nombre d'amp�rages diff�rents possibles (tableau de constantes)
#define NB_AMPERAGES_TRAITES 5


// On conserve l'amp�rage, le voltage, l'�tat (ALLUME ou NETEINT)
// et la demande en courant sur le circuit.
typedef struct{

    //En watt
    double ampere;

    //ON - OFF
    int etat;

    // Puissance de la demande actuelle
    // du circuit
    double demande_du_circuit;

    // TENSION_PHASE ou TENSION_ENTREE
    int tension;

}t_disjoncteur;

// Retourne un disjoncteur initialis� aux valeurs fournies.
t_disjoncteur* nouveau_disjoncteur(int ampere, int tension);

// Retourne la puissance maximum possible pour le disjoncteur.
// Ne doit pas �tre NULL.  Aucune validation.
int puissance_max(const t_disjoncteur* disjoncteur);


#endif